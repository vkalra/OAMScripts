#!/bin/bash
# This script will create a new APS agreement
# Make sure that both the master and clone admin servers are running!

# Got this jsonval script from https://gist.github.com/cjus/1047794 - Works great!
function jsonval {
    temp=`echo $json | sed 's/\\\\\//\//g' | sed 's/[{}]//g' | awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]}' | sed 's/\"\:\"/\|/g' | sed 's/[\,]/ /g' | sed 's/\"//g' | grep -w $prop`
    echo ${temp##*|}
}

# Setup
userPass=user.1:password
masterDc=dc2.ateam.com:7001
masterDcName=dc2East
cloneDcName=dc1West
replicationName=DC22DC1
pollInterval="60"

echo
echo Make sure replication is enabled
echo
# Output should look something like:
#curl -s -u "$userPass" "http://$masterDc/oam/services/rest/_replication/hello"
#  {"adminContext":"true","featureEnabled":"true","ok":"true"}
json=`curl -s -u "$userPass" "http://$masterDc/oam/services/rest/_replication/hello"`
prop='ok'
ok=`jsonval`
prop='featureEnabled'
enabled=`jsonval`
prop='adminContext'
adminContext=`jsonval`

# Test if replication is enabled 
if [ $ok == 'true' ] && [ $enabled == 'true' ] && [ $adminContext == 'true' ]
then
  # Debug json sample output 
  #  json={"enabled":"true","identifier":"201510160547491349","ok":"true","pollInterval":"900","startingSequenceNumber":"1761","state":"READY"}

  echo
  echo Create the agreement and get the agreement number
  echo
  echo curl -u "$userPass" -H 'Content-Type: application/json' -X POST "http://$masterDc/oam/services/rest/_replication/setup" -d  "{\"name\":\"$replicationName\",\"source\":\"$masterDcName\",\"target\":\"$cloneDcName\",\"documentType\":\"ENTITY\"}"
  json=`curl -s -u "$userPass" -H 'Content-Type: application/json' -X POST "http://$masterDc/oam/services/rest/_replication/setup" -d  "{\"name\":\"$replicationName\",\"source\":\"$masterDcName\",\"target\":\"$cloneDcName\",\"documentType\":\"ENTITY\"}"`

  # Debug
  echo The JSON call: $json

  # Check to make sure that you did not get this output {"ok":"false"}
  # Output looks something like this:
  #{"enabled":"true","identifier":"201510160547491349","ok":"true","pollInterval":"900","startingSequenceNumber":"1761","state":"READY"}

  prop='identifier'
  agreement=`jsonval`

  echo
  echo The New agreement is: $agreement
  # Check if agreement is not empty
  if [ -z "$agreement" ]
  then
    echo Error:  Agreement could not be created.
  else
    echo
    echo Shorten the poll interval to test with, default 3600 seconds setting to 60 seconds
    echo
  
    curl -u "$userPass" -H 'Content-Type: application/json'  -X PUT "http://$masterDc/oam/services/rest/_replication/$agreement" -d "{\"pollInterval\":\"$pollInterval\",\"replicaType\":\"CONSUMER\"}"
  fi
else
  echo Replication not enabled - Exiting
fi
