#!/bin/bash
# This script will  Disable/Delete APS agreement
# Get agreement Number; assumes only one agreement
# Make sure the master and clone admin servers are started!

# Got this jsonval script from https://gist.github.com/cjus/1047794 - Works great! 
function jsonval {
    temp=`echo $json | sed 's/\\\\\//\//g' | sed 's/[{}]//g' | awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]}' | sed 's/\"\:\"/\|/g' | sed 's/[\,]/ /g' | sed 's/\"//g' | grep -w $prop`
    echo ${temp##*|}
}

#  Setup
userPass=weblogic:Welcome1
masterDc=dc1.ateam.com:7001

# Get the agreement number
# Output looks something like this:
#{"featureEnabled":"true","identifiers":"201506122117005766","ok":"true"}
json=`curl -s -u "$userPass" "http://$masterDc/oam/services/rest/_replication/agreements"`
prop='identifiers'
agreement=`jsonval`

echo 
echo The Agreement is: $agreement
echo 

# Check if agreement is not empty
if [ -z "$agreement" ]
then
  echo There is no agreement to remove - Done
else
  echo Disabling the consumer agreement 
  echo 
  curl -u "$userPass" -H 'Content-Type: application/json' -X PUT "http://$masterDc/oam/services/rest/_replication/$agreement" -d '{"enabled":"false","replicaType":"CONSUMER"}'

  echo Disabling the supplier agreement
  curl -u "$userPass" -H 'Content-Type: application/json' -X PUT "http://$masterDc/oam/services/rest/_replication/$agreement" -d '{"enabled":"false","replicaType":"SUPPLIER"}'

  echo 
  echo Deleting the consumer supplier agreement
  echo 
  curl -u "$userPass" -H 'Content-Type: application/json' -X DELETE "http://$masterDc/oam/services/rest/_replication/$agreement"
fi
