#!/u01/oamr2ps3/Oracle/Middleware/Oracle_IDM1/common/bin/wlst.sh 

currentMaster="dc1.ateam.com:7001" 
currentClone="dc2.ateam.com:7001" 

# Modify the Clone DC 
connect('weblogic', 'Welcome1', 't3://' + currentClone) 
domainRuntime() 

# Change the Clone site to Master - No restart needed 

# Change write flag to true 
setMultiDataCenterWrite(WriteEnabledFlag="true") 

# Set type to Master 
setMultiDataCenterType(DataCenterType="Master") 


# Modify the Master DC 
connect('weblogic', 'Welcome1', 't3://' + currentMaster) 
domainRuntime() 

# Set master to clone 
setMultiDataCenterType(DataCenterType="Clone") 

# Set read only flag 
setMultiDataCenterWrite(WriteEnabledFlag="false") 

print "Now you must create a replication agreement on the new Master site and restart the Clone admin server and all managed servers"
