# This is a REST API front end for Automatic Policy Syncronization (APS)
# You can create, remomve, and verify the APS agreement for OAM 11g PS3
# This script was tested on python 2.7.13
# Vinay Kalra - Oracle A-Team

import os
import json
import urllib
import urllib2
import base64
import argparse
import smtplib
from datetime import datetime

config={}
global isLog

def log_request(request):
    """ Log the request to STDOUT or File  """
    headers=""
    for h in request.header_items():
        headers = headers + str(h)
    print "["+str(datetime.now())+ "]" + " | " + request.get_method() + " | " + request.get_full_url() + " | " +  "headers: " + headers + " | " + str(request.get_data())


def log_response(response):
    """Log the response to STDOUT or File"""
    print "["+str(datetime.now())+ "]" + " | " + str(response)
    
    
def send_request(url, creds=None, headers=None, data=None, method=None):
    """ Send the HTTP request and return the response """
    if data:
        request=urllib2.Request(url, data, headers)
    else:
        # Investigate why you cannot pass headers here???
        request=urllib2.Request(url, data=data)
        
    request.add_header("Authorization", "Basic %s" % creds)
    
    # Support for DELETE and PUT methods
    if method == "PUT" or method== "DELETE":
        request.get_method = lambda: method
        
    if isLog: log_request(request)
    try:
        response=urllib2.urlopen(request).read()
        if isLog: log_response(response)
        return response
    except urllib2.URLError, e:
        print e
    except urllib2.HTTPError, e:
        print "HTTP ERROR: " + e
    
    
def create_aps():
    """ Create the APS agreement between one or more clones. If the agreement already exists, it's ok, the REST call is re-entrant."""
    helloURL=str(config['master']['host']) + ":" + str(config['master']['port']) + "/oam/services/rest/_replication/hello"
    setupURL=str(config['master']['host']) + ":" + str(config['master']['port']) + "/oam/services/rest/_replication/setup"
    creds=base64.b64encode('%s:%s' % (config['master']['userName'],config['master']['password']))
    
    # Check to see if replication is turned on
    response=send_request(helloURL,creds)
    response = json.loads(response)

    if (response['ok'] == 'true' and response['adminContext'] == 'true' and response['featureEnabled'] == 'true'):
        print "APS> Replication is enabled..."
        
        header={'Content-Type':'application/json'}
        master=str(config['master']['clusterID'])

        # Iterate through all clones and create the agreement and set poll interval
        for c in config['clones']:
            replName = str(c['replicationName'])
            target = str(c['clusterID'])
            data = json.dumps({"name":replName,"source":master,"target":target,"documentType":"ENTITY"})
            
            # Create the agreement
            response=json.loads(send_request(setupURL, creds, header, data))
            
            # If a clone does not exists then you get {ok:false} check!
            if response['ok'] != 'false':
                # Change poll interval
                agreement=str(response['identifier'])
                agreementURL=str(config['master']['host']) + ":" + str(config['master']['port']) + "/oam/services/rest/_replication/" + agreement
                pollInterval=str(c['pollInterval'])
                data=json.dumps({"pollInterval":pollInterval,"replicaType":"CONSUMER"})
                response=send_request(agreementURL, creds, header, data, 'PUT')
                send_notification("Created agreement: " + agreement + " with poll interval of " + pollInterval) 
            else:
                send_notification("Invalid Clone configuration or Replication agreement already exits - " + str(data))
    else:
        print ("APS> Replication is not enabled.")


def remove_aps():
    """ This function will disable and delete one or more agreements."""
    agreementURL=str(config['master']['host']) + ":" + str(config['master']['port']) + "/oam/services/rest/_replication/agreements"
    consumerData=json.dumps({"enabled":"false","replicaType":"CONSUMER"})
    supplierData=json.dumps({"enabled":"false","replicaType":"SUPPLIER"})
    header={'Content-Type':'application/json'}
    creds=base64.b64encode('%s:%s' % (config['master']['userName'],config['master']['password']))
    
    # Get the agreements
    response=send_request(agreementURL,creds)
    response=json.loads(response)
    
    # Check if agreement is empty
    if 'identifiers' in response:
        # Log a bug where the this property should return an array with either one or many IDs
        if type(response['identifiers']) is unicode:
            idArr=[response['identifiers']]
        else:
            idArr=response['identifiers']
            #print idArr

        for a in idArr:
            disableURL=str(config['master']['host']) + ":" + str(config['master']['port']) + "/oam/services/rest/_replication/" + str(a)
        
            # Disable both the consumer and supplier agreement
            response=send_request(disableURL,creds, header, consumerData, 'PUT')
            response=send_request(disableURL,creds, header, supplierData, 'PUT')
        
            # Now let's delete the agreement
            response=send_request(disableURL,creds, header, None, 'DELETE')
            
        send_notification("Removing agreement(s): " + str(json.dumps(idArr)))
    else:
        send_notification("There is no agreement to remove.")
    

def modify_aps():
    """ This function will modify the current agreement """
    
    
def send_notification(text, printOnly=1):
    """ Send a notification e-mail to senders defined in smtp property in the aps.json file.
        You can sprinkle this call throughout the code if you want more notifications.
        If smtp is true and you still want to avoid sending e-mail, set the printOnly
        parameter to 1 (default); setting printOnly to 0 will send the e-mail as well. """
        
    # If printOnly
    if printOnly:
        # Print to STDOUT
        print ("APS> " + text)
    else:
        # Check if smtp is on
        if config['smtp']['running'] != 'false':
            # Import the email modules we'll need
            from email.mime.text import MIMEText
        
            msg = MIMEText(text)
            msg['Subject'] = str(config['smtp']['Subject'])
            msg['From'] = str(config['smtp']['From'])
            # Loop through e-mails
            #    If using a single e-mail address make sure you still place the e-mail in brakets [<emailAddress>] in the aps.json file!!
            listOfEmails=""
            lenOfEmails=len(config['smtp']['To'])
            count=0
            for to in config['smtp']['To']:
                if count == lenOfEmails-1:
                    listOfEmails = listOfEmails + str(to)
                else:
                    listOfEmails = listOfEmails + str(to) + ", "
                count += 1
        
                # Send the message via our own SMTP server, but don't include the
                # envelope header.
                s = smtplib.SMTP(str(config['smtp']['smtphost']))
                s.sendmail(str(config['smtp']['From']), to, msg.as_string())
                s.quit()
                # Print to STDOUT as well
                print ("APS> " + text)
    
def verify_aps():
    """ verify_aps - This function will check the last known change and compary with the clone servers.
        If there are any issues a notification e-mail will be sent using """
    agreementURL=str(config['master']['host']) + ":" + str(config['master']['port']) + "/oam/services/rest/_replication/agreements"
    header={'Content-Type':'application/json'}
    creds=base64.b64encode('%s:%s' % (config['master']['userName'],config['master']['password']))

    # Get the agreements
    response=send_request(agreementURL,creds)
    response=json.loads(response)
    
    # Check if agreement is not empty
    if 'identifiers' in response:
        # Log a bug where the this property should return an array with either one or many IDs
        if type(response['identifiers']) is unicode:
            idArr=[response['identifiers']]
        else:
            idArr=response['identifiers']
            #print idArr
            
        for currentAgreement in idArr:
            startNumberURL=str(config['master']['host']) + ":" + str(config['master']['port']) + "/oam/services/rest/_replication/" + str(currentAgreement)
        
            # Get the startine sequence number
            response=send_request(startNumberURL,creds)
            response=json.loads(response)
        
            lastSequenceURL=str(config['master']['host']) + ":" + str(config['master']['port']) + "/oam/services/rest/_replication/" + \
            str(currentAgreement) + "/latest?since=" + str(response['startingSequenceNumber'])
        
            # Get the startine sequence number
            response=send_request(lastSequenceURL,creds)
            response=json.loads(response)
    
            # return if there were no changes
            if response['ok'] != 'true':
               send_notification("There have been no changes.")
               continue
        
            lastSequenceCloneNumber = response['lastChange']['sequence']
        
            #print "last=" + lastSequenceCloneNumber
            lastSequenceCloneURL=str(config['master']['host']) + ":" + str(config['master']['port']) + "/oam/services/rest/_replication/" + \
            str(currentAgreement) + "?type=consumer"
        
            # Get the startine sequence number
            response=send_request(lastSequenceCloneURL,creds)
            response=json.loads(response)
        
            if 'lastSequenceNumber' in response:
                if response['lastSequenceNumber'] == lastSequenceCloneNumber:
                    send_notification(str(config['smtp']['APSSyncMsg']) + "The last sequence number is " + str(lastSequenceCloneNumber))
                else:
                    send_notiification(str(config['smtp']['APSNotSyncMsg'])) + "The last sequence number on Master/Clone is " + str(response['lastSequenceNumber']) + "/" + str(lastSequenceCloneNumber)
            else:
                send_notification("Cannot find clone's last applied sequence number.  You may need to apply one-off patch 23146778")
    else:
        send_notification("There are no agreements to verify.") 


def setup_config():
    """ Setup the configuration by reading the aps.json file."""
    # Check if aps.json exists
    fname="./aps.json"
    
    if os.path.isfile(fname):
        try:
            # Open the file and read
            with open(fname, 'r') as f:
                global config
                config = json.load(f)
                #print config
        except IOError as e:
            send_notification("Unable to open file or does not exists")

# Main
arguments = argparse.ArgumentParser()
arguments.add_argument('-c', action='store_true', help='Create the APS Agreement')
arguments.add_argument('-r', action='store_true', help='Remove the APS Agreement')
arguments.add_argument('-v', action='store_true', help='Verify the APS Agreement')
arguments.add_argument('-nolog', action='store_true', help='Turn off loging to STDOUT')
arguments.add_argument('-version', action='version', version='%(prog)s 1.0')
args = arguments.parse_args()

#print args

setup_config()

if args.nolog:
    isLog=0
else:
    isLog=1
if args.c: create_aps()
if args.r: remove_aps()
if args.v: verify_aps()